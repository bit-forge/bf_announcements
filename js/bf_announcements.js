(function($) {
  // Feature detect + local reference
  var storage = (function() {
    var uid = new Date;
    var storage;
    var result;
    try {
      (storage = window.localStorage).setItem(uid, uid);
      result = storage.getItem(uid) == uid;
      storage.removeItem(uid);
      return result && storage;
    } catch (exception) {}
  }());

  var showPopup = function(block_id, delay, cookie_length, type) {
    setTimeout(function() {
      var showPopup = false;
      var storage_key = "popup_block_shown";
      if(storage) {
        var last_shown;
        if (cookie_length == 0) {
          // Is a "session" announcement - should only be shown once per browsing session
          // Use sessionStorage instead of localStorage so that the value does not persist for "session" announcements
          storage = window.sessionStorage;
          // If the sessionStorage key does not exist and the value is not 'true' then show the popup;
          // Otherwise it has already been displayed this session
          if ( storage.getItem(storage_key) != 'null' && storage.getItem(storage_key) != 'true' ) {
            storage.setItem(storage_key, 'true');
            showPopup = true;
          }
        } else if(last_shown = storage.getItem(storage_key)) {
          if(parseInt(last_shown) < Date.now() - (60*60*24*1000*cookie_length)) {
            storage.setItem(storage_key, Date.now());
            showPopup = true;
          }
        } else {
          storage.setItem(storage_key, Date.now());
          showPopup = true;
        }
      } else {
        if(!$.cookie(storage_key)) {
          if ( cookie_length == 0 ) {
            $.cookie(storage_key, "viewed");
          } else {
            $.cookie(storage_key, "viewed", { expires: cookie_length });
          }
          showPopup = true;
        }
      }
      if (showPopup) {
        var $block = $("#" + block_id);

        if (type == 'popup') {
          $("#"+block_id).show();
          $.colorbox({href:$("#"+block_id), inline:true, opacity: 0.20, width: '600px', maxWidth:'100%', onClosed: function() {
            $("#"+block_id).hide();
          }});
        }
        if (type == 'slideup') {
          $block.prepend('<a class="close" href="#close">X</a>');

          $block.css({bottom: -$block.outerHeight() + "px"});

          $block.find('.close').on('click', function() {
            $block.animate({bottom: -$block.outerHeight()-20 + "px"}, 250, function() {
              $('body').removeClass('js-announcement');
              $block.hide();
            });
          });

          $block.show();
          $block.animate({bottom: "0px"}, 1000);
          $('body').addClass('js-announcement');
        }
      }
    }, delay*1000);
  };

  $(function() {
    var block_id = Drupal.settings.bf_announcements.block_id;
    if(block_id) {
      var $block = $('#' + block_id);
      if($block.length > 0) {
        $block.hide();
        if(Drupal.settings.bf_announcements.enabled) {
          showPopup(block_id, Drupal.settings.bf_announcements.delay, Drupal.settings.bf_announcements.cookie_length, Drupal.settings.bf_announcements.type);
        }
      }
    }
  });
})(jQuery);
